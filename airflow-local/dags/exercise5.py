import requests, os, json, re
import pandas as pd
from sqlalchemy import create_engine
from airflow.utils.dates import days_ago
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.operators.bash import BashOperator

urlPath = 'https://opendata.ecdc.europa.eu/covid19/testing/json/'
conn_string = 'postgresql://airflow:airflow@airflow-local_postgres_1:5432/airflow'

with DAG(
        'Exercise5',
        default_args={'retries': 0},
        description='Exercise 5 DAG',
        start_date=days_ago(1),
        schedule_interval=None,
        template_searchpath='/opt/airflow/dags',
        catchup=False,
        tags=['Exercise 5: Enrich Data'],
) as dag:
    dag.doc_md = __doc__


def create_connections(conn_string):
    db = create_engine(conn_string, encoding='utf-8')
    conn = db.connect()
    return conn


def json_to_df(urlPath):
    r = requests.get(urlPath)
    json_data = r.json()
    df = pd.DataFrame(json_data)
    return df

#I chose to enrich my data with information about Data on testing for COVID-19 by week and country
# link : https://www.ecdc.europa.eu/en/publications-data/covid-19-testing
#here I am creating a new table based on these data and on exercise 5b (next pipeline) , I'm creating a View about the number of deaths per covid test
def my_func(**kwargs):
    conn = create_connections(conn_string=kwargs['conn_string'])
    df = json_to_df(urlPath=kwargs['urlPath'])
    df['year_week'] = df['year_week'].map(lambda x: re.sub(r'W', '', x))
    df['extract_date'] = pd.Timestamp(year=2022, month=9, day=12).strftime("%d-%m-%Y")
    df.to_sql('covid_testing_data', con=conn, if_exists='replace',
              index=False)

##Operators

start = BashOperator(task_id='start', bash_command='date', dag=dag)

end = BashOperator(task_id='end', bash_command='date', dag=dag)

Enrich_Data = PythonOperator(task_id='Enrich_Data', python_callable=my_func,
                             op_kwargs={'conn_string': conn_string, 'urlPath': urlPath},
                             provide_context=True, dag=dag)

start >> Enrich_Data >> end
