import psycopg2
from airflow.utils.dates import days_ago
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.operators.bash import BashOperator

conn_string = 'postgresql://airflow:airflow@airflow-local_postgres_1:5432/airflow'

with DAG(
        'Exercise3',
        default_args={'retries': 0},
        description='Exercise 3 DAG',
        start_date=days_ago(1),
        template_searchpath='/opt/airflow/dags',
        catchup=False,
        tags=['Exercise 3: Create a View'],
) as dag:
    dag.doc_md = __doc__

def my_func():
    conn = psycopg2.connect(conn_string)
    cur = conn.cursor()
    cur.execute("""CREATE OR REPLACE VIEW "Cumulative_number_for_14_days_of_COVID-19_cases_per_100000" AS SELECT a.*, 
    b.year_week,b.cumulative_count, b.extract_date FROM countries_data a LEFT JOIN covid_data_updated b ON a."Country"=b.country WHERE 
    b.indicator='cases' AND b.year_week='2020-35' 
    """)
    conn.commit()
    conn.close()


##Operators
start = BashOperator(task_id='start', bash_command='date', dag=dag)

end = BashOperator(task_id='end', bash_command='date', dag=dag)

Creating_View = PythonOperator(task_id='Creating_View', python_callable=my_func,
                               provide_context=True, dag=dag)

start >> Creating_View >> end
