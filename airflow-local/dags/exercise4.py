import psycopg2
from airflow.utils.dates import days_ago
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.operators.bash import BashOperator

conn_string = 'postgresql://airflow:airflow@airflow-local_postgres_1:5432/airflow'

with DAG(
        'Exercise4',
        default_args={'retries': 0},
        description='Exercise 4 DAG',
        start_date=days_ago(1),
        template_searchpath='/opt/airflow/dags',
        catchup=False,
        tags=['Exercise 4: Queries'],
) as dag:
    dag.doc_md = __doc__


def my_func():
    conn = psycopg2.connect(conn_string)
    cur = conn.cursor()
    #Query1 : Luxemburg
    cur.execute("""select *, (weekly_count/population) as Covid_Cases from covid_data_updated where year_week='2020-31' and indicator='cases'
                    order by (weekly_count/population) desc """)
    print('-------------------------------------------------------------------------------------------')
    print('Query1 results: Luxemburg')
    print('Explain & Analysis for Query1--> Query complete in 86 msec, Sort(rows=30 loops=1) ')
    print('-------------------------------------------------------------------------------------------')
    print('-------------------------------------------------------------------------------------------')
    #Query2 : Hungary,Latvia,Finland,Slovakia,Estonia,Lithuania,Italy,Norway,Slovenia,Liechtenstein
    cur.execute("""select *, (weekly_count/population) as Covid_Cases from covid_data_updated where year_week='2020-31' and indicator='cases'
                    order by (weekly_count/population) asc limit 10 """)
    print('Query2 results: Hungary,Latvia,Finland,Slovakia,Estonia,Lithuania,Italy,Norway,Slovenia,Liechtenstein')
    print('Explain & Analysis for Query2--> Query complete in 111 msec, Limit (rows=10 loops=1), Sort(rows=10 loops=1) ')
    print('-------------------------------------------------------------------------------------------')
    print('-------------------------------------------------------------------------------------------')
    #Query3 : France,Germany,Netherlands,Austria,Belgium,Denmark,Norway,Ireland,Luxembourg,Iceland
    cur.execute("""select SUM(weekly_count), a.country, b."GDP ($ per capita)"  from covid_data_updated a 
                    left join 
                    (select "Country", "GDP ($ per capita)" from countries_data
                    order by "GDP ($ per capita)" desc limit 22) b 
                    on a.country=b."Country"
                    where indicator='cases' and b."GDP ($ per capita)" is not null
                    group by a.country, b."GDP ($ per capita)" 
                    order by SUM(weekly_count) desc
    """)
    print('Query3 results: France,Germany,Netherlands,Austria,Belgium,Denmark,Norway,Ireland,Luxembourg,Iceland '
          '-from top 22 countries-')
    print('Explain & Analysis for Query3--> Query complete in 66 msec, Sort (rows=10 loops=1), Aggregate (rows=10 '
          'loops=1) ' 
          "Buckets: Batches: Memory Usage: 37 kB,  Hash Inner Join (rows=1400 loops=1), "
          "Seq Scan on covid_data_updated as a (rows=4200 loops=1), "
          "Hash (rows=21 loops=1),  Subquery Scan (rows=21 loops=1), Limit (rows=22 loops=1), Sort (rows=22 loops=1),"
          "Seq Scan on countries_data as countries_data (rows=227 loops=1)")
    print('-------------------------------------------------------------------------------------------')
    print('-------------------------------------------------------------------------------------------')

    #Query4 : WESTERN EUROPE, BALTICS, NEAR EAST, EASTERN EUROPE
    cur.execute("""select a."Region",cast(b.weekly_count/1000000 as double precision), a."Pop. Density (per sq. mi.)" from countries_data a left join
                    covid_data_updated b on a."Country"=b.country
                    where b.year_week='2020-31' and b.indicator='cases'
                    group by cast(b.weekly_count/1000000 as double precision), a."Region", a."Pop. Density (per sq. mi.)" 
                    order by cast(b.weekly_count/1000000 as double precision) """)
    print('Query4 results: WESTERN EUROPE, BALTICS, NEAR EAST, EASTERN EUROPE')
    print('Explain & Analysis for Query4--> Query complete in 110 msec,Group (rows=29 loops=1),  Sort (rows=29 '
          'loops=1),  Hash Inner Join (rows=29 loops=1), '
          'Seq Scan on covid_data_updated as b (rows=30 loops=1), Hash (rows=227 loops=1)')
    print('-------------------------------------------------------------------------------------------')
    print('-------------------------------------------------------------------------------------------')

    #Query5 : 185 rows have duplicated records
    cur.execute("""SELECT country, weekly_count,year_week,cumulative_count,source,rate_14_day, COUNT(*)
                FROM covid_data_updated
                GROUP BY country, weekly_count,year_week,cumulative_count,source,rate_14_day 
                HAVING COUNT(*) > 1 """)
    print('Query5 results: 185 rows have duplicated records')
    print('Explain & Analysis for Query5--> Query complete in 63 msec, Aggregate (rows=185 loops=1), Seq Scan on covid_data_updated as covid_data_updated (rows=8400 loops=1)  ')
    print('-------------------------------------------------------------------------------------------')
    print('-------------------------------------------------------------------------------------------')
    print('Improvement Suggestions: Use of a Partition Key such as extract date or year_week for bigger tables. '
          'In our case, because of limited volume data we can use indexing.  ')
    print('-------------------------------------------------------------------------------------------')
    conn.commit()
    conn.close()


##Operators
start = BashOperator(task_id='start', bash_command='date', dag=dag)

end = BashOperator(task_id='end', bash_command='date', dag=dag)

Queries = PythonOperator(task_id='Queries', python_callable=my_func,
                               provide_context=True, dag=dag)

start >> Queries >> end
