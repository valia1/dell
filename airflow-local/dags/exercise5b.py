import psycopg2
from airflow.utils.dates import days_ago
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.operators.bash import BashOperator

conn_string = 'postgresql://airflow:airflow@airflow-local_postgres_1:5432/airflow'

with DAG(
        'Exercise5b',
        default_args={'retries': 0},
        description='Exercise 5b DAG',
        start_date=days_ago(1),
        template_searchpath='/opt/airflow/dags',
        catchup=False,
        tags=['Exercise 5b: Create a View based on enriched data'],
) as dag:
    dag.doc_md = __doc__

#Based on our new dataset we can create similar queries and find useful information about covid-19
#Here we can se a View about the number of deaths per covid test
def my_func():
    conn = psycopg2.connect(conn_string)
    cur = conn.cursor()
    cur.execute("""CREATE OR REPLACE VIEW "DEATH_RATE_PER_COVID_TEST" AS SELECT a.country, a.new_cases, 
    a.tests_done, b.weekly_count, (b.weekly_count/a.tests_done) as death_rate_per_tests from covid_testing_data a 
    left join covid_data_updated b on a.country=b.country and a.year_week=b.year_week where b.indicator='deaths'""")
    conn.commit()
    conn.close()


##Operators
start = BashOperator(task_id='start', bash_command='date', dag=dag)

end = BashOperator(task_id='end', bash_command='date', dag=dag)

Creating_View_Enriched_Data = PythonOperator(task_id='Creating_View_Enriched_Data', python_callable=my_func,
                               provide_context=True, dag=dag)

start >> Creating_View_Enriched_Data >> end
