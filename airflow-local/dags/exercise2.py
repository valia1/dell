import requests
import pandas as pd
import datetime as dt
from sqlalchemy import create_engine
from airflow.utils.dates import days_ago
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.operators.bash import BashOperator

urlPath = 'https://opendata.ecdc.europa.eu/covid19/nationalcasedeath/json/'
conn_string = 'postgresql://airflow:airflow@airflow-local_postgres_1:5432/airflow'


####the pipeline is scheduled to run once per day(via schedule_interval)
with DAG(
        'Exercise2',
        default_args={'retries': 0},
        description='Exercise 2 DAG',
        start_date=days_ago(1),
        schedule_interval="@daily",
        template_searchpath='/opt/airflow/dags',
        catchup=False,
        tags=['Exercise 2: Create a Pipeline'],
) as dag:
    dag.doc_md = __doc__


def create_connections(conn_string):
    db = create_engine(conn_string)
    conn = db.connect()
    return conn


def json_to_df(urlPath):
    r = requests.get(urlPath)
    json_data = r.json()
    df = pd.DataFrame(json_data)
    return df

####saving json data on df1, using table covid_data from exercise 1 as df2, concat df1 and df2 and drop the duplicates
#### saving new data to df3 and saving it as a new table named covid_data_updated
def my_func(**kwargs):
    conn = create_connections(conn_string=kwargs['conn_string'])
    df1 = json_to_df(urlPath=kwargs['urlPath'])
    #add timestamp column with today's date
    cols = list(df1.columns)
    df1['extract_date'] = dt.datetime.now().strftime("%d-%m-%Y")
    df2 = pd.read_sql_query('select * from "covid_data"', con=conn)
    df3 = pd.concat([df1,df2],axis=0).sort_values('extract_date').drop_duplicates(cols, keep='first').reset_index(drop=True)
    df3.to_sql('covid_data_updated', con=conn, if_exists='replace',
               index=False)

##Operators
start = BashOperator(task_id='start', bash_command='date', dag=dag)

end = BashOperator(task_id='end', bash_command='date', dag=dag)

Creating_Pipeline = PythonOperator(task_id='Creating_Pipeline', python_callable=my_func,
                                   op_kwargs={'conn_string': conn_string, 'urlPath': urlPath},
                                   provide_context=True, dag=dag)

start >> Creating_Pipeline >> end
