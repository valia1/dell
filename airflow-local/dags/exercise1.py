import requests, os ,json
import pandas as pd
from sqlalchemy import create_engine
from airflow.utils.dates import days_ago
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.operators.bash import BashOperator

urlPath = 'https://opendata.ecdc.europa.eu/covid19/nationalcasedeath/json/'
conn_string = 'postgresql://airflow:airflow@airflow-local_postgres_1:5432/airflow'

with DAG(
        'Exercise1',
        default_args={'retries': 0},
        description='Exercise 1 DAG',
        start_date=days_ago(1),
        schedule_interval=None,
        template_searchpath='/opt/airflow/dags',
        catchup=False,
        tags=['Exercise 1: Loading Data'],
) as dag:
    dag.doc_md = __doc__


def create_connections(conn_string):
    db = create_engine(conn_string,encoding='utf-8')
    conn = db.connect()
    return conn


def json_to_df(urlPath):
    r = requests.get(urlPath)
    json_data = r.json()
    df = pd.DataFrame(json_data)
    return df


def my_func(**kwargs):
    conn = create_connections(conn_string=kwargs['conn_string'])
    df = json_to_df(urlPath=kwargs['urlPath'])
    df['extract_date'] = pd.Timestamp(year=2022, month=9, day=9).strftime("%d-%m-%Y")
    df.to_sql('covid_data', con=conn, if_exists='replace',
              index=False)
    cwd = os.getcwd()
    df1 = pd.read_csv(f'{cwd}/dags/countries.csv')
    cat_cols = list(df1.select_dtypes(['object']).columns)
    df1[cat_cols] = df1[cat_cols].apply(lambda x:x.str.strip() if x is not None else x)
    df1.to_sql('countries_data', con=conn, if_exists='replace',
               index=False)


##Operators

start = BashOperator(task_id='start', bash_command='date', dag=dag)

end = BashOperator(task_id='end', bash_command='date', dag=dag)

Loading_Data = PythonOperator(task_id='Loading_Data', python_callable=my_func,
                              op_kwargs={'conn_string': conn_string, 'urlPath': urlPath},
                              provide_context=True, dag=dag)

start >> Loading_Data >> end
