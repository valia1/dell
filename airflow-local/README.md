# Run Airflow using docker containers

<h2>Description</h2>

This project creates a full working Airflow environment using Docker containers.

It uses the Celery Executor with Redis as the broker/backend and PostgreSQL as the core Airflow database.

The webserver forwards the port 8080 to the host, so you can go to localhost:8080 after starting the project.

****Requirements:****  
1. Install Docker: _https://docs.docker.com/engine/install/ubuntu/_
2. Run the commands as shown below
3. Set the PgAdmin Properties as shown below
4. Set the Airflow Connections as shown below


<h2>RUN</h2>

    mkdir airflow-local

    cd airflow-local

    mkdir -p ./dags ./logs ./plugins ./scripts

    chmod -R +x ./dags ./logs ./plugins ./scripts

    echo -e "AIRFLOW_UID=$(id -u)\nAIRFLOW_GID=0" > .env

    docker-compose up

<h2>Links</h2>

Airflow Link: http://localhost:8080/

    --------Login to Airflow----------
    username: airflow and password: airflow
    ----------------------------------
    Please, inore all the default DAG examples
    except the ones with the exercises.
    
PgAdmin Page: http://localhost:15432/

    --------Login to PgAdmin----------
    username: valia.6@hotmail.com and password: airflow
<h2>PgAdmin Properties<h2>
<h4>_On the tab General_<h4>

    Name:localhost

<h4>_On the tab Connection_<h4>

    Host: airflow-local_postgres_1 
    (name of the container created in the docker image postgres, you can find it using the command "docker ps")

    Port: 5432

    Maintenance database: airflow

    Username: airflow

    Password: airflow


<h2>Setting Connections on airflow:<h2>
    

<h3>Postgres</h3>
*_Admin->Connections->Add Connection_*

    Basic Configs:
    Conn Id * : postgres_local
    Conn Type *: Postgres
    Host: airflow-local_postgres_1
    Schema: airflow
    Login: airflow
    Password: airflow


---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
<h1>Exercise 4: Queries</h1>
    
    We can execute all the queries from exercise 4 on airflow as a pipeline 
    and check the results from DAG logs & on PgAdmin. 
    -----------------------------------------------------------------------

    The results are shown below too:
    -----------------------------------------------------------------------
    QUERY 1: 
```sql
    select *, (weekly_count/population) as Covid_Cases from covid_data_updated where year_week='2020-31' and indicator='cases'
                    order by (weekly_count/population) desc 
```

    Query1 results: Luxemburg
    Explain & Analysis for Query1--> Query complete in 86 msec, Sort(rows=30 loops=1) 
    -----------------------------------------------------------------------
    QUERY 2: 
```sql
    select *, (weekly_count/population) as Covid_Cases from covid_data_updated where year_week='2020-31' and indicator='cases'
                    order by (weekly_count/population) asc limit 10
```
    Query2 results: Hungary,Latvia,Finland,Slovakia,Estonia,Lithuania,Italy,Norway,Slovenia,Liechtenstein
    Explain & Analysis for Query2--> Query complete in 111 msec, Limit (rows=10 loops=1), Sort(rows=10 loops=1)
    -----------------------------------------------------------------------
    QUERY 3: 
```sql
    select SUM(weekly_count), a.country, b."GDP ($ per capita)"  from covid_data_updated a 
                    left join 
                    (select "Country", "GDP ($ per capita)" from countries_data
                    order by "GDP ($ per capita)" desc limit 22) b 
                    on a.country=b."Country"
                    where indicator='cases' and b."GDP ($ per capita)" is not null
                    group by a.country, b."GDP ($ per capita)" 
                    order by SUM(weekly_count) desc
```
    Query3 results: France,Germany,Netherlands,Austria,Belgium,Denmark,Norway,Ireland,Luxembourg,Iceland -from top 22 countries-
    Explain & Analysis for Query3--> Query complete in 66 msec, Sort (rows=10 loops=1), Aggregate (rows=10 loops=1) 
    Buckets: Batches: Memory Usage: 37 kB,  Hash Inner Join (rows=1400 loops=1), 
    Seq Scan on covid_data_updated as a (rows=4200 loops=1), 
    Hash (rows=21 loops=1),  Subquery Scan (rows=21 loops=1), Limit (rows=22 loops=1), Sort (rows=22 loops=1),
    Seq Scan on countries_data as countries_data (rows=227 loops=1)
    -----------------------------------------------------------------------
    QUERY 4: 
```sql
    select a."Region",cast(b.weekly_count/1000000 as double precision), a."Pop. Density (per sq. mi.)" from countries_data a left join
                    covid_data_updated b on a."Country"=b.country
                    where b.year_week='2020-31' and b.indicator='cases'
                    group by cast(b.weekly_count/1000000 as double precision), a."Region", a."Pop. Density (per sq. mi.)" 
                    order by cast(b.weekly_count/1000000 as double precision)
```
    Query4 results: WESTERN EUROPE, BALTICS, NEAR EAST, EASTERN EUROPE
    Explain & Analysis for Query4--> Query complete in 110 msec,Group (rows=29 loops=1),  Sort (rows=29 loops=1),  Hash Inner Join (rows=29 loops=1), 
    Seq Scan on covid_data_updated as b (rows=30 loops=1), Hash (rows=227 loops=1)
    -----------------------------------------------------------------------
    QUERY 5: 
```sql
    SELECT country, weekly_count,year_week,cumulative_count,source,rate_14_day, COUNT(*)
                FROM covid_data_updated
                GROUP BY country, weekly_count,year_week,cumulative_count,source,rate_14_day 
                HAVING COUNT(*) > 1
```
    Query5 results: 185 rows have duplicated records
    Explain & Analysis for Query5--> Query complete in 63 msec, Aggregate (rows=185 loops=1), Seq Scan on covid_data_updated as covid_data_updated (rows=8400 loops=1) 

    <h2>Exercise 6 </h2>

        Exercise 6 is located in scripts folder
